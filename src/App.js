import React, { useEffect, useState } from 'react'
// import axios from 'axios'
//import Pagination from '../src/Pagination';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

let PageSize = 10;

function App() {
  const [posts, setPosts] =useState([1])
    

    const ApiFetch = () => {
      fetch("https://gorest.co.in/public/v1/users")    
        .then(response => {
          return response.json()
        })
        .then(data => {  
          setPosts(data.data)   
             
          })
    }
    useEffect(() => {
      ApiFetch()
    }, [])

  return (
    <div className="container"> 
          <div className="row mt-5 mb-5">
              <div className="col-md-3">
                <div>
                  Total Users
                </div>
              </div>
              <div className="col-md-3">
              <div>
                  Total Males
                </div>
              </div>
              <div className="col-md-3">
              <div>
                  Total Females
                </div>
              </div>
              <div className="col-md-3">
              <div>
                   Total Active
                </div>
              </div>
          </div>
         <table className='table'>
           <thead>
             <tr>
              <td>SL.NO</td>
              <td>NAME</td>
              <td>EMAIL</td>
              <td>GENDER</td>
              <td>STATUS</td>
             </tr>
           </thead>
           <tbody>
            {  posts.map((post,index) => (
            <tr>
              <td>{index+1}</td>
              <td>{post.name}</td>
              <td>{post.email}</td>
              <td>{post.gender}</td>
              <td>{post.status}</td>
            </tr>
            ))}
           </tbody>
         </table>
         {/* <Pagination
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={data.length}
          pageSize={PageSize}
          onPageChange={page => setCurrentPage(page)}
        /> */}
    </div>
  )
}

export default App;









